package playerHandling;

import java.util.ArrayList;

import cardHandling.Card;

/**
 * Creates a person, with an empty arraylist that will soon be the persons picked cards.
 * @author Johan
 */

public abstract class Person {
	
	protected ArrayList<Card> pickedCards;

	public Person () {

		pickedCards = new ArrayList<Card>();
		
	}

	public ArrayList<Card> getPickedCards() {
		return pickedCards;
	}
	
	public void GetCard(Card card) {
		
		pickedCards.add(card);
	}
	/**
	 * Empties the persons hand.
	 * @param person - (dealer/player)
	 */
	public void clearHand (Person person) {
		
		person.pickedCards.clear();
	}
	
}