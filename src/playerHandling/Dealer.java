package playerHandling;

import cardHandling.Card;
import cardHandling.CardDeck;

/**
 * The dealer has two arraylists. One with the dealers picked cards.
 * The other one is cards that the dealer gets from CardDeck-class,
 * and then gives to the player or the dealer itself.
 * @author Johan
 */
public class Dealer extends Person {
	
	protected CardDeck dealersCardDeck;
	
	public Dealer () {
		
		dealersCardDeck = new CardDeck();
	}
	
	/**
	 * The dealer gives a card to a person. Gets the card from getRandomCard-method.
	 * @param person
	 * @returns a card.
	 */
	public Card GiveCardTo (Person person) {
		
		Card pickedCard = dealersCardDeck.getRandomCard();

		person.GetCard(pickedCard);
		return pickedCard;
	}
}