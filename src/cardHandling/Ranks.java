package cardHandling;

/**
 * Creates constants for the ranks, all with an int value.
 * @author Johan
 */
public enum Ranks {
	
	Two(2),
	Three(3),
	Four(4),
	Five(5),
	Six(6),
	Seven(7),
	Eight(8),
	Nine(9),
	Ten(10),
	Jack(10),
	Queen(10),
	King(10),
	Ace(11);				//Ace value is 11, but can be 1 point if person otherwise would bust..
	
	private int value;
	
	Ranks (int rankValue) {
		value = rankValue;
	}

	public int getValue() {
		return value;
	}
}