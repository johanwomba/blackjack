package cardHandling;

/**
 * Holds card values.
 * @author Johan
 * 
 */
public class Card {

	private Suits suit;
	private Ranks rank;

	public Card (Suits suits, Ranks ranks) {

		this.suit = suits;
		this.rank = ranks;

	}
	
	public Suits getSuit() {
		return suit;
	}

	public Ranks getRank() {
		return rank;
	}
}