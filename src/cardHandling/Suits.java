package cardHandling;

/**
 * Creates constants for all the suits, with no particular values.
 * @author Johan
 */
public enum Suits {
	
	Spades,
	Clubs,
	Diamonds,
	Hearts;

}