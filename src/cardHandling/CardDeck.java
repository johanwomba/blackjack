package cardHandling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class CardDeck {

	private ArrayList<Card> cardDeck;		//holds all 52 cards.
	private ArrayList<Card> usedCards;		//when a card is used in game, put it in this array.
	private Random randomNumber;

	public CardDeck () {

		randomNumber = new Random();
		usedCards = new ArrayList<Card>();
		cardDeck = new ArrayList<Card>();
		createDeck();
		shuffleDeck();
	}

	/**
	 * Creates all the suits and ranks (creates all the cards)
	 * and then add the cards to the cardDeck arraylist.
	 */
	public void createDeck() {
		usedCards.clear();
		cardDeck.clear();

		for (Suits s : Suits.values()) {
			for (Ranks r : Ranks.values()) {
				Card c = new Card(s, r);
				cardDeck.add(c);
			}
		}
	}
	
	/**
	 * Shuffles the card deck.
	 */
	private void shuffleDeck() {
		Collections.shuffle(this.cardDeck); 
	}
	/**
	 * Picks out a random card from the deck.
	 * And also removes the card from the deck. (puts it in the usedCards arraylist.)
	 * Also, if the card deck is empty - creates and shuffles a new one.
	 * @returns a random card.
	 */
	public Card getRandomCard() {
		
		try {

			if (cardDeck.isEmpty()) {
				createDeck();
				shuffleDeck();
			}

			Card randomCard = cardDeck.remove(findPosition(0, cardDeck.size() - 1));
			if (randomCard != null) {
				usedCards.add(randomCard);

			}
			return randomCard;

		} catch (Exception ex) {
			return null;
		}
	}

	private int findPosition(int min, int max) {
		int randomNum = randomNumber.nextInt((max - min) + 1) + min;
		return randomNum;
	}

}