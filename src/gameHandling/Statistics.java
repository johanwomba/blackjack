package gameHandling;

import java.text.DecimalFormat;

public class Statistics {
	
	/**
	 * Calculates the percentage chance of getting blackjack with two cards.
	 * @param blackjackCounter <- How many blackjacks where received?
	 * @param howManyTimes <- How many deals were played?
	 */
	public static void quickGameStats (int blackjackCounter, int howManyTimes) {

		System.out.println("You got blackjack on two cards " + blackjackCounter
				+ " times on " + howManyTimes + " deals.");
		
		double percentCalc = blackjackCounter * 100;
		double toPercent = percentCalc / howManyTimes;
		
		//use decimalFormat to truncate to two decimals.
		System.out.println("The percentage chance of getting blackjack is: " + new DecimalFormat("##.##").format(toPercent) + "%"); 

	}
	
	/**
	 * Show regular game stats, fix this later.
	 * @param - total games, total wins, total blackjacks, total dealer wins, total tied games.
	 */
	public static void regularGameStats () {
		
		System.out.println("Total games played: ");
		System.out.println("Total wins: ");
		System.out.println("Total blackjacks: ");
		System.out.println("Total dealer wins: ");
		System.out.println("Total tied games: ");
	}
}