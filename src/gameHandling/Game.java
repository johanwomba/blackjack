package gameHandling;

import java.util.ArrayList;
import java.util.Scanner;

import cardHandling.Card;
import cardHandling.CardDeck;
import cardHandling.Ranks;
import playerHandling.Dealer;
import playerHandling.Player;

public class Game {

	public Player player;
	public Dealer dealer;
	public CardDeck cardDeck;

	static Scanner input = new Scanner (System.in);
	public static boolean wannaContinue;


	public Game() {
		this.player = new Player();
		this.dealer = new Dealer();
		this.cardDeck = new CardDeck();
		wannaContinue = true;
	}

	/**
	 * The first thing the player chooses. 
	 * !!!IMPLEMENT FACTORY PATTERN!!!
	 * http://www.tutorialspoint.com/design_pattern/factory_pattern.htm
	 * @param newGame
	 */
	public void welcomeScreen (Game newGame) {
		System.out.println("Welcome to Blackjack!");

		while (true) {
			System.out.println("Would you like to play a (1) probability game, (2) regular game or (3) exit program?");
			String firstChoice = input.nextLine();

			if (firstChoice.equals("1")) {

				//game starts
				do {
					newGame.quickGame();
				} while (Game.playAgain());
			}
			else if (firstChoice.equals("2")) {

				//game starts
				do {
					newGame.newRound();
					//Statistics.regularGameStats();
				} while (Game.playAgain());

			}
			else if (firstChoice.equals("3")) {
				System.out.println("Bye bye now!");
				break;
			}
			else {
				System.out.println("Didn't catch that, only the provided numbers please.");
			}
		}
	}

	/**
	 * Launches a quick game where the only objective is to get 21 on two cards.
	 * The player inputs how many deals they would like.
	 */
	public void quickGame () {

		try {
			System.out.println("This mode is designed to calculate the procentual chance to get blackjack with only 2 cards.");
			System.out.println("How many deals would you like? (tips - minimum 100 deals.)");
			
			int howManyTimes;
			howManyTimes = input.nextInt();
			input.nextLine();					//use nextLine after nextInt so the "enter" or space doesn't mess everything up. (basically.)
			
			int blackjackCounter = 0;

			for (int i = 0; i < howManyTimes; i++) {

				Card card = dealer.GiveCardTo(player);
				this.cardInfo(card);
				card = dealer.GiveCardTo(player);
				this.cardInfo(card);
				if (checkPoints(player.getPickedCards()) == 21) {
					blackjackCounter++;
				}
				player.clearHand(player);		//empties the players hand.
			}
			//print the result when all deals are done.
			Statistics.quickGameStats(blackjackCounter, howManyTimes);
		}
		catch (Exception ex) {
			System.out.println("Error!" + "\n" + "Only positive numbers without decimal please.");
		}
	}
	/**
	 * Launches a regular game. The dealer gives 2 cards to player and himself, get asked to hit or stay,
	 * the dealer tries to beat the player, and finally shows who won.
	 */
	public void newRound() {

		System.out.println("You and the dealer will start with two cards each.");
		System.out.println("If you get an ace, the program will automatically calculate if its worts 1 or 11 points." + "\n");

		//Gives two cards to the player, and then prints the score.
		Card card = dealer.GiveCardTo(player);
		this.cardInfo(card);
		card = dealer.GiveCardTo(player);
		this.cardInfo(card);
		printPlayerScore();

		//Gives two cards to the dealer, and then prints the score.
		card = dealer.GiveCardTo(dealer);
		this.cardInfo(card);
		card = dealer.GiveCardTo(dealer);
		this.cardInfo(card);
		printDealerScore();

		//hit or stay?
		while (hitOrstay()) {
			card = dealer.GiveCardTo(player);
			this.cardInfo(card);
			printPlayerScore();

			//if player gets blackjack or busts, break out of loop.
			if (checkPoints(player.getPickedCards()) >= 21) {
				break;
			}
		}
		//dealers turn while player stands.
		while (checkPoints(dealer.getPickedCards()) <= 17) {
			card = dealer.GiveCardTo(dealer);
			this.cardInfo(card);
			printDealerScore();
		}

		//print out who won!
		whoWon();

		//clears the score of player and dealer.
		dealer.clearHand(dealer);
		player.clearHand(player);

	}

	/**
	 * Checks if player wants to hit or stay.
	 * @returns true if hit, false if stay.
	 */
	public boolean hitOrstay() {
		
		boolean hitOrGo = false;
		System.out.println("Do you want to hit or stay? (h/s)");
		String hitStay = input.nextLine();
		
		if (hitStay.equalsIgnoreCase("h")) {
			hitOrGo = true;
		}
		else if (hitStay.equalsIgnoreCase("s")) {
			hitOrGo = false;
		}
		return hitOrGo;
	}

	/**
	 * Calculates the score of the person who asks for method.
	 * @param pickedCards <- the persons picked cards.
	 * @returns the persons current score.
	 */
	public int checkPoints(ArrayList<Card> pickedCards) {

		int totalPoints = 0;

		for (int i = 0; i < pickedCards.size(); i++) {

			int cardValue = pickedCards.get(i).getRank().getValue();
			totalPoints += cardValue;
			
			//if the person gets an ace and will bust, count as 1 point instead of eleven.
			if (pickedCards.get(i).getRank().equals(Ranks.Ace) && totalPoints > 21) { 
				totalPoints -= 10;
			}
		}
		return totalPoints;
	}

	/**
	 * Prints the players current score.
	 */
	public void printPlayerScore() {

		System.out.println("Your score is: " + checkPoints(player.getPickedCards()) + "\n");
	} 

	/**
	 * Prints the dealers score.
	 */
	public void printDealerScore() {

		System.out.println("The dealers score is: " + checkPoints(dealer.getPickedCards()) + "\n");
	}

	/**
	 * Checks the points after the game is done, and prints who won.
	 */
	public void whoWon () {

		if (checkPoints(player.getPickedCards()) > 21){
			System.out.println("You lost.");
		}
		else if (checkPoints(player.getPickedCards()) == 21){
			System.out.println("Blackjack!");
		}
		else if (checkPoints(dealer.getPickedCards()) > 21 && (checkPoints(player.getPickedCards()) <= 21)) {
			System.out.println("You won!");
		}
		else if (checkPoints(player.getPickedCards()) < checkPoints(dealer.getPickedCards())
				&& checkPoints(dealer.getPickedCards()) <= 21) {
			System.out.println("Dealer won.");
		}
		else if (checkPoints(player.getPickedCards()) == checkPoints(dealer.getPickedCards())) {
			System.out.println("It's a tie.");
		}
		else if (checkPoints(player.getPickedCards()) > checkPoints(dealer.getPickedCards())
				&& checkPoints(player.getPickedCards()) <= 21) {
			System.out.println("You won!");
		}
	}

	/**
	 * prints the suit and rank of the picked card.
	 * @param pickedCard - the card chosen from getRandomCard method.
	 */
	public void cardInfo(Card pickedCard) {

		System.out.println("The card is a " + pickedCard.getRank().toString() + " of " + pickedCard.getSuit().toString());
	}

	/**
	 * Checks if player wants to continue playing.
	 * @returns true as long as the player wants to keep playing.
	 * 
	 */
	public static boolean playAgain () {

		System.out.println("Would you like to play again? (y/n)");
		String playAgain = input.nextLine();

		if (playAgain.equalsIgnoreCase("n")) {
			System.out.println("Returning to main menu.");
			wannaContinue = false;
		} 
		else if (playAgain.equalsIgnoreCase("y")){
			wannaContinue = true;
		}
		return wannaContinue;
	}
}